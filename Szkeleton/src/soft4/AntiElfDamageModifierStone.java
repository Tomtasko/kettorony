package soft4;

/**
 * Duplasebz�st ad elfek ellen
 * @author Povazson
 *
 */
public class AntiElfDamageModifierStone extends DamageModifierStone {
	/**
	 * T�rp�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�rp�k ellen
	 */
	public int damageModifierOnDwarf(){
		return 1;
	}
	/**
	 * Emberek elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� emberek ellen
	 */
	public int damageModifierOnHuman(){
		return 1;
	}
	/**
	 * T�nd�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�nd�k ellen
	 */
	public int damageModifierOnElf(){
		return 2;
	}
	/**
	 * Hobbitok elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� hobbitok ellen
	 */
	public int damageModifierOnHobbit(){
		return 1;
	}
	
	public int getMana() {
		return PTC.AntiElfMana;
	}		
}
