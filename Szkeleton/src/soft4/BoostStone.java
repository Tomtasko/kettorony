package soft4;

/**
 * 
 * A toronyba helyezhet�, annak tulajdons�gait jav�t� k�vek �soszt�lya
 * @author Povazson
 */
public abstract class BoostStone {
	/**
	 * Konstruktor
	 */
	public BoostStone() {
	}
	
	/**
	 * A tulajdons�gm�dos�t�s szignat�r�ja.
	 * A lesz�rmazott oszt�lyok k�telesek megval�s�tani, amennyiben nem absztraktak maguk is.
	 * @param t A jav�tand� torony referenci�ja
	 */
	public abstract void setTowerAttribute(Tower t);
	public abstract int getMana();
}
