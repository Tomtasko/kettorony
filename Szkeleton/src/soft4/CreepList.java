package soft4;

import java.util.ArrayList;
/**
 * Az ellens�geket tartalmaz� lista.
 * @author Povazson
 *
 */
public class CreepList {
	/**
	 * az el� �s a m�r halott de m�g nem t�r�lt ellens�gek list�i
	 */
	private ArrayList<Creep> creeps,deads;
	/**
	 * a j�t�kos referenci�ja
	 */
	private Player player;
	/**
	 * a map, amin l�teznek a creepek.
	 */
	private MapData map;
	
	/**
	 * Konstruktor
	 * @param pl A j�t�kos
	 * @param mp a p�lya adatai
	 */
	public CreepList(Player pl, MapData mp) {
		creeps = new ArrayList<Creep>();
		deads = new ArrayList<Creep>();
		player = pl;
		map = mp;
	}
	
	/**
	 * A meghal� ellens�gek e f�ggv�nnyel regisztr�lj�k hal�lukat.
	 * @param creep az ellens�g, aki �pp meghal
	 */
	public void imDead(Creep creep){
		deads.add(creep);
	}
	
	/**
	 * A list�k t�rl�se, valamint �j ellens�gek gener�l�sa.
	 */
	public void initialize(){
		creeps.clear();
		deads.clear();
	}
	
	/**
	 * Minden �l� ellens�g <code>move</code> f�ggv�ny�nek h�v�sa.
	 */
	public void moveAll(){
		for(Creep c:creeps){
			c.move();
		}
	}
	
	/**
	 * A konstukt�lhat�s�ghoz
	 * @param p player
	 */
	public void setPlayer(Player p){
		player = p;
	}
	
	public Player getPlayer(){
		return player;
	}
	/**
	 * A meghalt ellens�gek kit�rl�se,
	 * valamint a j�t�kos man�j�nak n�vel�se minden halott ut�n.
	 * Ha minden �l� ellens�g meghalt, akkor a j�tlkos nyer.
	 */
	public void removeDead(){
		for(Creep c:deads){
			player.changeMana(10);
			creeps.remove(c);
			deads.remove(c);
		}
		if(creeps.size()==0){
			player.victory();
			return;
		}
	}
	
	public void addCreep(Creep c){
		creeps.add(c);
		c.name = "Creepr " + creeps.size()+": ";
	}

}
