package soft4;

/**
 * A sebz�sm�dos�t� k� �soszt�lya.
 * A b�nusz n�lk�li sebz�st jelenti, ha p�ld�nyos�tva van.
 * @author Povazson
 *
 */
public class DamageModifierStone {
	/**
	 * T�rp�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�rp�k ellen
	 */
	public int damageModifierOnDwarf(){
		return 1;
	}
	/**
	 * Emberek elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� emberek ellen
	 */
	public int damageModifierOnHuman(){
		return 1;
	}
	/**
	 * T�nd�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�nd�k ellen
	 */
	public int damageModifierOnElf(){
		return 1;
	}
	/**
	 * Hobbitok elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� hobbitok ellen
	 */
	public int damageModifierOnHobbit(){
		return 1;
	}
	
	public int getMana() {
		return 0;
	}		
}
