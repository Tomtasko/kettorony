package soft4;
/**
 * T�nde, ellens�gt�pus
 * @author Povazson
 *
 */
public class Elf extends Creep {
	/**
	 * Konstruktor, az �s megval�s�tja.
	 * @see soft4.Creep#Creep(CreepList, RouteSegment)
	 */
	public Elf(int hp, CreepList c, RouteSegment r,int tts) {
		super(hp, c, r, tts);
	}

	/**
	 * A t�nde sebz�d�se.
	 * Ha meghal, regisztr�lja mag�t halottk�nt a list�j�n.
	 * Ha oszt�dik, nem sebz�dik, hanem megfelezi az �let�t, �s gener�l egy �j ellens�get.
	 * @param damage az alap sebz�s
	 * @param ammo a sebz�sm�dos�t� k� referenci�ja
	 */
	@Override
	public void hurt(int damage, DamageModifierStone ammo) {
		switch(PTC.multiplied){
		case 0:
			boolean splitting = (Math.random()*100>85);
			if(splitting){
				System.out.println(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
				PTC.appendOutputToFile(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
				hp=(int)Math.ceil(hp/2);
				System.out.println(name+"Oszt�dtam.");
				PTC.appendOutputToFile(name+"Oszt�dtam.");
				Elf tmp = clone();
				context.addCreep(tmp);
				activeSegment.addCreep(tmp);
			}
			else{
				hp-=damage*ammo.damageModifierOnDwarf();
				System.out.println(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
				PTC.appendOutputToFile(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
				if(hp<=0) {
					System.out.println(name+"Meghaltam");
					activeSegment.removeCreep(this);
					context.imDead(this);
				}
			}
			break;
		case 1:
			System.out.println(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
			PTC.appendOutputToFile(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
			hp=(int)Math.ceil(hp/2);
			System.out.println(name+"Oszt�dtam.");
			PTC.appendOutputToFile(name+"Oszt�dtam.");
			Elf tmp = clone();
			context.addCreep(tmp);
			activeSegment.addCreep(tmp);
			break;
		case 2:
			hp-=damage*ammo.damageModifierOnDwarf();
			System.out.println(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
			PTC.appendOutputToFile(name+"Sebz�dtem "+damage+" * "+ammo.damageModifierOnElf()+" �rt�ket.");
			if(hp<=0) {
				System.out.println(name+"Meghaltam");
				activeSegment.removeCreep(this);
				context.imDead(this);
			}	
		}
	}

	@Override
	public Elf clone(){
		Elf tmp = new Elf(hp, context,activeSegment,nextMoveIn);
		return tmp;
	}
}
