package soft4;

import java.util.ArrayList;
/**
 * A p�lya alkot�elemeinek �soszt�lya.
 * A j�t�kos ezekkel v�gezhet interakci�kat.
 * @author Povazson
 *
 */
public abstract class Field {
	/**
	 * prot� teszthez
	 */
	public String name;
	/**
	 * a mez� szomsz�dai
	 */
	protected ArrayList<Field> neighbours;
	/**
	 * a mez�re mutat� �tszegmensek list�ja.
	 */
	protected ArrayList<RouteSegment> routeSegments;
	/**
	 * a j�t�kos referenci�ja
	 */
	protected Player player;
	
	/**
	 * x koordin�ta, a routesegmentek elnevez�s�hez kell
	 */
	public int x;
	
	/**
	 * y koordin�ta, a routesegmentek elnevez�s�hez kell
	 */
	public int y;
	
	/**
	 * Konstruktor.
	 * L�rtehoz egy �j list�t az �tszegmenseknek, �s a szomsz�dainak.
	 */
	public Field(){
		routeSegments = new ArrayList<>();
		neighbours = new ArrayList<>();
	}
	
	/**
	 * Elemet ad a <code>Field</code> szomsz�dai k�z�.
	 * @param f az �j szomsz�d
	 * @see Field
	 */
	public void addNeighbour(Field f){
		neighbours.add(f);
	}
	
	/**
	 * Elemet ad a <code>Field</code>>re mutat� <code>RouteSegment</code>>ek list�j�ba.
	 * @param r a hozz�adand� <code>RouteSegment</code>
	 * @see soft4.Creep
	 * @see soft4.RouteSegment
	 */
	public void addRouteSegment(RouteSegment r){
		routeSegments.add(r);
	}
	/*
	 * A k�z�s kezelhet�s�g miatt l�tez� a f�ggv�nyek.
	 * A lesz�rmazottak megval�s�tj�k �ket, ha nem vonatkozik r�juk, akkor <code>false</code>- al t�rnek vissza.
	 * @param boostStone
	 * @return
	 */
	public abstract boolean addBoostStoneToTower(BoostStone boostStone);
	public abstract boolean addDamageModifierStoneToTower(DamageModifierStone modStone);
	public abstract boolean addStoneToRoadBlock(RoadBlockStone stone);
	public abstract boolean buildTower(int dmg, int firerate, int range);
	public abstract boolean buildRoadBlock();
}
