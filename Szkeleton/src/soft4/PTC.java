package soft4;

import java.io.*;
import java.util.*;
import java.text.*;

//PROTO TEST CONTROL
public class PTC {

	private static String datum;
	private static MapData mapData;
	private static TowerList towerList;
	private static CreepList creepList;
	private static Player player;
	private static Weather weather;
	private static Timer timer;
	
	public static DamageModifierStone alap = new DamageModifierStone();
	public static DamageModifierStone antiDwarf = new AntiDwarfDamageModifierStone();
	public static DamageModifierStone antiElf = new AntiElfDamageModifierStone();
	public static DamageModifierStone antiHobbit = new AntiHobbitDamageModifierStone();
	public static DamageModifierStone antiHuman = new AntiHumanDamageModifierStone();
	public static BoostStone damageBoost = new DamageStone();
	public static BoostStone fireBoost = new SpeedStone();
	public static BoostStone rangeBoost = new RangeStone();
	public static RoadBlockStone rbStone = new RoadBlockStone();
	public static int speed;
	public static int blocked_speed;
	/**
	 * 0 � v�letlenszer� k�dk�pz�d�s, 1- �lland� k�d, 2 � sosincs k�d
	 */
	public static int fog;
	/**
	 * 0 � v�letlenszer� k�dhossz, 0+ - a megadott id�tartamig tart a k�d.
	 */
	public static int fog_time;
	/**
	 * 0 � v�letlenszer� �szrev�tel, k�d eset�n, 1 � mindig �szrevesz k�d eset�n, 2 � sosem vesz �szre k�d eset�n
	 */
	public static int covered;
	/**
	 * 0 � v�letlenszer� ir�nyv�laszt�s el�gaz�skor, 0+ - a megadott elemet v�lasztja a next list�b�l (ha nincs ilyen sorsz�m� elem, akkor az utols�t v�lasztja)
	 */
	public static int move_seek;
	/**
	 * 0 � v�letlenszer� oszt�d�s sebz�s hat�s�ra, 1 � mindig oszt�dik, 2 � sosem oszt�dik
	 */
	public static int multiplied;
	public static int roundTime = 1;
	public static String map_data_Filename;
	public static String route_data_Filename;
	public static String adjacency_Filename;
	public static ArrayList<RouteSegment> segedRouteSegments;
	public static ArrayList<Field> segedFields;
	public static int waveDelay = 10;
	public static int towerMana;
	public static int roadblockMana;
	public static int speedStoneMana;
	public static int rangeStoneMana;
	public static int dmgStoneMana;
	public static int AntiDwarfMana;
	public static int AntiElfMana;
	public static int AntiHobbitMana;
	public static int AntiHumanMana;
	public static int roadblockStoneMana;
	public static int manabyround = 100;
	public static int tick = 0;

	public static void main(String[] args) throws IOException {
		segedFields = new ArrayList<>();
		towerList = new TowerList();
		weather = new Weather();
		mapData = new MapData(towerList,weather);
		timer = new Timer(player,towerList,creepList,weather);
		player = new Player(mapData,towerList,creepList,timer);
		timer.setPlayer(player);
		creepList = new CreepList(player,mapData);

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Date date = new Date();
		datum = dateFormat.format(date);

		PTC ptc = new PTC();
		ptc.start();
	}
	
	
	/* A beg�pelt sz�veg beolvas�sa, majd a megfelel� f�ggv�ny megh�v�sa
	 * @throws IOException
	 */
	private void start() throws IOException{
		
		
		BufferedReader in=new BufferedReader(
                new InputStreamReader(System.in));
        
        prompt();
        String line;
        while((line=in.readLine())!=null){
        
            String[] splits=line.split(" ");
        
            if(splits[0].equals("mapLoad")){
                
                mapLoad(splits);
                
            }
            else if(splits[0].equals("buildTower")){
            
            	buildTower(splits);
                
            }else if(splits[0].equals("buildRoadBlock")){
            
            	buildRoadBlock(splits);
                
            }else if(splits[0].equals("addHuman")){
            
            	addHuman(splits);
                
            }else if(splits[0].equals("addHobbit")){
            
            	addHobbit(splits);
                
            }else if(splits[0].equals("addDwarf")){
            
            	addDwarf(splits);
            }else if(splits[0].equals("addElf")){
            
            	addElf(splits);
            }else if(splits[0].equals("setMana")){
            
            	setMana(splits);
            }else if(splits[0].equals("addDamageModifierStone")){
            
            	addDamageModifierStone(splits);
            }else if(splits[0].equals("addDamageStone")){
            
            	addDamageStone(splits);
            }else if(splits[0].equals("addRangeStone")){
            
            	addRangeStone(splits);
            }else if(splits[0].equals("addRoadblockStone")){
            
            	addRoadblockStone(splits);
            }else if(splits[0].equals("addSpeedStone")){
            
            	addSpeedStone(splits);
            }else if(splits[0].equals("onTimer_tick")){
            
            	onTimer_tick(splits);
            }else if(splits[0].equals("newGame")){
            
            	newGame(splits);
            }else if(splits[0].equals("addWave")){
            
            	addWave(splits);
            }else if(splits[0].equals("setActiveField")){
            
            	setActiveField(splits);
            }else if(splits[0].equals("exit")){
            
            	System.exit(0);
            }else {
            	System.out.println("Hiba, nincs ilyen parancs!");
            }
            
            prompt();
        }
        
        in.close();
        
    }

	/** Integer ellen�rz� f�ggv�ny.
	 * Vizsg�lja, hogy a megadott string lehet-e integer, valamint nagyobb-e nulla.
	 * A m�sodik param�terrel megadhatjuk, hogy mekkor�n�l nagyobb integert nem fogadunk el.
	 * @param split : A megadott string amelyet integerr� akarunk konvert�lni
	 * @param x: az a sz�m amelyn�l nagyobb �rt�ket nem fogadunk el. 
	 */
	public boolean checkInt(String split, int x){
		try{
			int e= Integer.valueOf(split);
			if(e<0) return false;
			else if (e>x) return false;
			else return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}

	public boolean checkInt(String split){
		try{
			int e= Integer.valueOf(split);
			if(e<0) return false;
			else return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}

	public boolean checkInt(String split, String m){
		try{
			Integer.valueOf(split);
			return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}
	

	/**
	 * Az elfogadott parancsok f�jlba �r�sa
	 * @param command : Az elfogadott parancs kieg�sz�tve a parancs kiad�s�nak pontos idej�vel
	 * @throws IOException
	 */
	private void appendCommandsToFile(String[] command){
		
		try{
			FileWriter fileLog = null;
			BufferedWriter log = null;

			String sumCommand = "";
			for(int i=0; i< command.length; i++){
				sumCommand= sumCommand + command[i] + " ";
			}
		
			try{
				fileLog = new FileWriter(datum+"-bemenet.txt",true);
				log = new BufferedWriter(fileLog);
				
				log.write(sumCommand);
				log.newLine();
			}
			catch(FileNotFoundException ex){
				System.out.println("Nem tudta megnyitni a f�jlt!");
			}
			finally{
				try{
					if(log!=null)
						log.close();
				}
				catch(IOException ex){
					System.out.println("Hiba a bemenet f�jl kezel�se k�zben!");
				}
			}
		} catch (IOException e) {
			System.out.println("Hiba a f�jl megnyit�sa k�zben!");
		}
		
	}

	public static void appendOutputToFile(String output){
		
		try{
			FileWriter fileLog = null;
			BufferedWriter log = null;

			try{
				fileLog = new FileWriter(datum+"-kimenet.txt",true);
				log = new BufferedWriter(fileLog);
				
				log.write(output);
				log.newLine();
			}
			catch(FileNotFoundException ex){
				System.out.println("Nem tudta megnyitni a f�jlt!");
			}
			finally{
				try{
					if(log!=null)
						log.close();
				}
				catch(IOException ex){
					System.out.println("Hiba a kimenet f�jl kezel�se k�zben!");
				}
			}
		} catch (IOException e) {
			System.out.println("Hiba a f�jl megnyit�sa k�zben!");
		}
		
	}
	
	

    /**
     * Az id�z�t�t null�ba �ll�tja, t�rli az ellens�g �s toronylist�t, legener�lja a map-ot. Be�ll�tja a v�letlenszer� d�nt�sek determinisztikuss�g�t:
     * <fog>: 0 � v�letlenszer� k�dk�pz�d�s, 1- �lland� k�d, 2 � sosincs k�d.
     * <fog_time>: 0 � v�letlenszer� k�dhossz, 0+ - a megadott id�tartamig tart a k�d.
     * <covered>: 0 � v�letlenszer� �szrev�tel, k�d eset�n, 1 � mindig �szrevesz k�d eset�n, 2 � sosem vesz �szre k�d eset�n.
     * <move_seek>: 0 � v�letlenszer� ir�nyv�laszt�s el�gaz�skor, 0+ - a megadott elemet v�lasztja a next list�b�l (ha nincs ilyen sorsz�m� elem, akkor az utols�t v�lasztja).
     * <multipled>: 0 � v�letlenszer� oszt�d�s sebz�s hat�s�ra, 1 � mindig oszt�dik, 2 � sosem oszt�dik.
	 *
     * @param splits
     */
	private void newGame(String[] splits) {
		if(splits.length==6){
			if(checkInt(splits[1],2) && checkInt(splits[2]) && checkInt(splits[3],2) && checkInt(splits[4]) && checkInt(splits[5],2))
			{
				appendCommandsToFile(splits);
				timer.set(0);
				towerList.deleteAll();
				if (mapData.initialize(map_data_Filename,route_data_Filename,adjacency_Filename)==0){
					creepList.initialize();
					fog = Integer.valueOf(splits[1]);
					fog_time = Integer.valueOf(splits[2]);
					covered = Integer.valueOf(splits[3]);
					move_seek = Integer.valueOf(splits[4]);
					multiplied = Integer.valueOf(splits[5]);
					weather.init();
					System.out.println("A j�t�k �jraind�tva.");
					appendOutputToFile("A j�t�k �jraind�tva.");
					timer.set(roundTime);
				}
				else {
					System.out.println("Az �j j�t�k ind�t�s sikertelen.");
					appendOutputToFile("Az �j j�t�k ind�t�s sikertelen.");					
				}
			}
			else{
				System.out.println("Valamelyik param�ter nem felel meg az el��r�soknak: \n <fog>: 0 � v�letlenszer� k�dk�pz�d�s, 1- �lland� k�d, 2 � sosincs k�d. \n <fog_time>: 0 � v�letlenszer� k�dhossz, 0+ - a megadott id�tartamig tart a k�d. \n <covered>: 0 � v�letlenszer� �szrev�tel, k�d eset�n, 1 � mindig �szrevesz k�d eset�n, 2 � sosem vesz �szre k�d eset�n. \n <move_seek>: 0 � v�letlenszer� ir�nyv�laszt�s el�gaz�skor, 0+ - a megadott elemet v�lasztja a next list�b�l (ha nincs ilyen sorsz�m� elem, akkor az utols�t v�lasztja). \n <multipled>: 0 � v�letlenszer� oszt�d�s sebz�s hat�s�ra, 1 � mindig oszt�dik, 2 � sosem oszt�dik.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <newGame> <fog> <fog_time> <covered> <move_seek> <multipled>");
		}
	}

	/**
	 * A param�terben kapott �rt�knek megfelel� k�rt szimul�l a j�t�kban.
	 * @param splits
	 */
	private void onTimer_tick(String[] splits) {
		if(splits.length==2){
			if(checkInt(splits[1])){
				appendCommandsToFile(splits);
				for(int i = 0; i<Integer.valueOf(splits[1]);i++){
					tick++;
					timer.onTimer();
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <onTimer_tick> <intRound>");
		}
	}
	
	/**
	 *Be�ll�tja a j�t�kos activefield v�ltoz�j�t az x, y param�terek �ltal meghat�rozott mez�re.
	 * @param splits
	 */
	private void setActiveField(String[] splits) {
		if(splits.length==3){
			if(checkInt(splits[1]) && checkInt(splits[2])){
				appendCommandsToFile(splits);
				int i = 0;
				while (!((Integer.valueOf(splits[1])==segedFields.get(i).x)&&(Integer.valueOf(splits[2])==segedFields.get(i).y))&&(i<segedFields.size()-1)){
					i++;
				}
				if (i<segedFields.size()-1){
					player.setActiveField(segedFields.get(i));
					System.out.println("Az activefield be�ll�tva "+segedFields.get(i).name+"-re.");
					appendOutputToFile("Az activefield be�ll�tva "+segedFields.get(i).name+"-re.");					
				}
				else {
					System.out.println("A be�ll�t�s sikertelen, nincs mez� a megadott koordin�t�n.");
					appendOutputToFile("A be�ll�t�s sikertelen, nincs mez� a megadott koordin�t�n.");	
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <setActiveField> <X> <Y>");
		}
	}

	/**
	 *Egy adott ellens�g hull�m l�trehoz�sa parancs. Param�terei sorban megadj�k
	 *a k�l�nb�z� fajta l�trehozott ellens�gek sz�m�t az els� param�terben megadott 
	 *hull�mban. Az ellens�gek helye az X, Y �s Offset sz�mok �ltal meghat�rozott poz�ci�ban lesz.
	 *
	 * @param splits
	 */
	private void addWave(String[] splits) {
		if(splits.length==13){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4]) && checkInt(splits[5]) && checkInt(splits[6]) &&
					checkInt(splits[7]) && checkInt(splits[8]) && checkInt(splits[9]) && checkInt(splits[10]) && checkInt(splits[11]) && checkInt(splits[12],7)){
				appendCommandsToFile(splits);
				int i = 0;
				String segeds = "Routesegment("+splits[10]+","+splits[11]+","+splits[12]+")";
				while (!(segeds.equals(segedRouteSegments.get(i).name))&&(i<segedRouteSegments.size()-1)){
					i++;
				}
				if (i<segedRouteSegments.size()-1){
					int j;
					Hobbit hobbit;
					Human human;
					Elf elf;
					Dwarf dwarf;
					for (j=0; j<Integer.valueOf(splits[2]);j++){
						hobbit = new Hobbit(Integer.valueOf(splits[6]),creepList,segedRouteSegments.get(i),Integer.valueOf(splits[1])*waveDelay);
						creepList.addCreep(hobbit);
						segedRouteSegments.get(i).addCreep(hobbit);
					}
					for (j=0; j<Integer.valueOf(splits[3]);j++){
						human = new Human(Integer.valueOf(splits[7]),creepList,segedRouteSegments.get(i),Integer.valueOf(splits[1])*waveDelay);
						creepList.addCreep(human);
						segedRouteSegments.get(i).addCreep(human);
					}
					for (j=0; j<Integer.valueOf(splits[4]);j++){
						dwarf = new Dwarf(Integer.valueOf(splits[8]),creepList,segedRouteSegments.get(i),Integer.valueOf(splits[1])*waveDelay);
						creepList.addCreep(dwarf);
						segedRouteSegments.get(i).addCreep(dwarf);
					}
					for (j=0; j<Integer.valueOf(splits[5]);j++){
						elf = new Elf(Integer.valueOf(splits[9]),creepList,segedRouteSegments.get(i),Integer.valueOf(splits[1])*waveDelay);
						creepList.addCreep(elf);
						segedRouteSegments.get(i).addCreep(elf);
					}
					
					
					System.out.println("L�trehozva "+splits[2]+" db hobbit (hp: "+splits[6]+"), "+splits[3]+" db ember (hp: "+splits[7]+"), "
							+splits[4]+" db dwarf (hp: "+splits[8]+"), "+splits[5]+" db elf (hp: "+splits[9]+")  a/az "+splits[1]
									+" hull�mhoz a/az "+splits[10]+" "+splits[11]+" "+splits[12]+" helyen.");
					appendOutputToFile("L�trehozva "+splits[2]+" db hobbit (hp: "+splits[6]+"), "+splits[3]+" db ember (hp: "+splits[7]+"), "
							+splits[4]+" db dwarf (hp: "+splits[8]+"), "+splits[5]+" db elf (hp: "+splits[9]+")  a/az "+splits[1]
									+" hull�mhoz a/az "+splits[10]+" "+splits[11]+" "+splits[12]+" helyen.");
				}
				else {
					System.out.println("A wave l�trehoz�sa nem siker�lt.");
					appendOutputToFile("A wave l�trehoz�sa nem siker�lt.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addWave> <waveNum> <hobbitNum> <humanNum> <dwarfNum> <elfNum> <hobbit_hp> <human_hp> <dwarf_hp> <elf_hp> <intX> <intY> <intOffset>");
		}
	}

	/**
	 * A param�terben kapott toronyhoz (sorsz�ma a towerlist-ben) gyors�t� k� rendel�se, a m�sodik param�ter a k� �ra.
	 * @param splits
	 */
	private void addSpeedStone(String[] splits){
		if(splits.length==3){
			if(checkInt(splits[1]) && checkInt(splits[2])){
				appendCommandsToFile(splits);
				speedStoneMana = Integer.valueOf(splits[2]);
				if (player.stoneToTower(fireBoost)) {
					System.out.println("A torony l�v�ssebess�ge megn�velve.");
					appendOutputToFile("A torony l�v�ssebess�ge megn�velve.");						
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addSpeedStone> <tower> <mana>");
		}
	}

	/**
	 * A param�terben kapott koordin�t�kon l�v� akad�lyhoz k� rendel�se, a harmadik param�ter a k� �ra.
	 * @param splits
	 */
	private void addRoadblockStone(String[] splits) {
		if(splits.length==4){
			if(checkInt(splits[1],1000) && checkInt(splits[2],1000) && checkInt(splits[3],100)){
				appendCommandsToFile(splits);
				int i = 0;
				while (!((Integer.valueOf(splits[1])==segedFields.get(i).x)&&(Integer.valueOf(splits[2])==segedFields.get(i).y))&&(i<segedFields.size()-1)){
					i++;
				}
				if (i<segedFields.size()-1){
					player.setActiveField(segedFields.get(i));
					roadblockStoneMana = Integer.valueOf(splits[3]);
					if (player.stoneToRoadBlock(rbStone)) {
						System.out.println("Az akad�ly meger�s�tve.");
						appendOutputToFile("Az akad�ly meger�s�tve.");					
					}
				}
				else {
					System.out.println("Nincs ilyen koordin�t�j� mez�.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addRoadblockStone> <X> <Y> <mana>");
		}
	}

	/**
	 * A param�terben kapott toronyhoz (sorsz�ma a towerlist-ben) range n�vel� k� rendel�se, a m�sodik param�ter a k� �ra.
	 * @param splits
	 */
	private void addRangeStone(String[] splits) {
		if(splits.length==3){
			if(checkInt(splits[1]) && checkInt(splits[2])){
				appendCommandsToFile(splits);
				rangeStoneMana = Integer.valueOf(splits[2]);
				if (player.stoneToTower(rangeBoost)) {
					System.out.println("A torony hat�t�vols�ga megn�velve.");
					appendOutputToFile("A torony hat�t�vols�ga megn�velve.");					
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addRangeStone> <tower> <mana>");
		}
	}
	
	/**
	 * A param�terben kapott toronyhoz (sorsz�ma a towerlist-ben) sebz� k� rendel�se, a m�sodik param�ter a k� �ra.
	 * @param splits
	 */
	private void addDamageStone(String[] splits) {
		if(splits.length==3){
			if(checkInt(splits[1],1000) && checkInt(splits[2],100)){
				appendCommandsToFile(splits);
				dmgStoneMana = Integer.valueOf(splits[2]);
				if (player.stoneToTower(damageBoost)) {
					System.out.println("A torony l�v�sereje megn�velve.");
					appendOutputToFile("A torony l�v�sereje megn�velve.");					
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addDamageStone> <tower> <mana>");
		}
	}
	
	/**
	 * A param�terben kapott torony sebz�st�pus�nak �ll�t�sa, a param�terben kapott ellens�g t�pus ellen, harmadik param�ter a k� �ra.
	 * @param splits
	 */
	private void addDamageModifierStone(String[] splits) {
		if(splits.length==4){
			if(checkInt(splits[1]) && checkInt(splits[2],4) && checkInt(splits[3])){
				appendCommandsToFile(splits);
				switch (Integer.valueOf(splits[2])) {
					case 1 : AntiDwarfMana = Integer.valueOf(splits[3]);
							 if (player.damageModifierStoneToTower(antiDwarf)) {
								 System.out.println("A k� hozz�ad�sa sikeres.");
								 appendOutputToFile("A k� hozz�ad�sa sikeres.");
							 }	 
							break;
					case 2 : AntiElfMana = Integer.valueOf(splits[3]);
					 		 if (player.damageModifierStoneToTower(antiElf)) {
					 			 System.out.println("A k� hozz�ad�sa sikeres.");
					 			 appendOutputToFile("A k� hozz�ad�sa sikeres.");
					 		 }	 
					 		 break;
					case 3 : AntiHobbitMana = Integer.valueOf(splits[3]);
							 if (player.damageModifierStoneToTower(antiHobbit)) {
								 System.out.println("A k� hozz�ad�sa sikeres.");
								 appendOutputToFile("A k� hozz�ad�sa sikeres.");
							 }	 
							 break;
					case 4 : AntiHumanMana = Integer.valueOf(splits[3]);
			 		 		 if (player.damageModifierStoneToTower(antiHuman)) {
			 		 			 System.out.println("A k� hozz�ad�sa sikeres.");
			 		 			 appendOutputToFile("A k� hozz�ad�sa sikeres.");
			 		 		 }	 
			 		 		 break;
					}
			
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addDamageModifierStone> <tower> <stoneType> <mana>");
		}
	}

	/**
	 * Be�ll�tja a j�t�kos man�j�t a param�terben kapott �rt�kre.
	 * @param splits
	 */
	private void setMana(String[] splits) {
		if(splits.length==2){
			if(checkInt(splits[1],"m�nusz")){
			appendCommandsToFile(splits);
				player.changeMana(Integer.valueOf(splits[1]));
				System.out.println("A mana �ll�tva "+splits[1]+" -al.");
				appendOutputToFile("A mana �ll�tva "+splits[1]+" -al.");
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <setMana> <intMana>");
		}
	}

	/**
	 * T�nde  ellens�g l�trehoz�sa parancs, adott �leter�vel adott helyre. 
	 * A megfelel� hely�t az x �s y koordin�t�k adj�k meg, az �ton bel�li hely�t a negyedik param�ter �rt�k adja (eltol�s). 
	 * @param splits
	 */
	private void addElf(String[] splits) {
		if(splits.length==5){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4],7) ){
				appendCommandsToFile(splits);
				int i = 0;
				String segeds = "Routesegment("+splits[2]+","+splits[3]+","+splits[4]+")";
				while (!(segeds.equals(segedRouteSegments.get(i).name))&&(i<segedRouteSegments.size()-1)){
					i++;
				}
				if (i<segedRouteSegments.size()-1){
					Elf elf = new Elf(Integer.valueOf(splits[1]),creepList,segedRouteSegments.get(i),segedRouteSegments.get(i).getSpeed());
					creepList.addCreep(elf);
					segedRouteSegments.get(i).addCreep(elf);
					System.out.println("L�trehozva egy "+splits[1]+" �leterej� elf a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
					appendOutputToFile("L�trehozva egy "+splits[1]+" �leterej� elf a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
				}
				else {
					System.out.println("L�trehoz�s sikertelen.");
					appendOutputToFile("L�trehoz�s sikertelen.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addElf> <hp> <intX> <intY> <intOffset>");
		}
	}

	/**
	 * T�rpe ellens�g l�trehoz�sa parancs, adott �leter�vel adott helyre. 
	 * A megfelel� hely�t az x �s y koordin�t�k adj�k meg, az �ton bel�li hely�t a negyedik param�ter �rt�k adja (eltol�s).
	 * @param splits
	 */
	private void addDwarf(String[] splits) {
		if(splits.length==5){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4],7) ){
				appendCommandsToFile(splits);
				int i = 0;
				String segeds = "Routesegment("+splits[2]+","+splits[3]+","+splits[4]+")";
				while (!(segeds.equals(segedRouteSegments.get(i).name))&&(i<segedRouteSegments.size()-1)){
					i++;
				}
				if (i<segedRouteSegments.size()-1){
					Dwarf dwarf = new Dwarf(Integer.valueOf(splits[1]),creepList,segedRouteSegments.get(i),segedRouteSegments.get(i).getSpeed());
					creepList.addCreep(dwarf);
					segedRouteSegments.get(i).addCreep(dwarf);
					System.out.println("L�trehozva egy "+splits[1]+" �leterej� t�rp a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
					appendOutputToFile("L�trehozva egy "+splits[1]+" �leterej� t�rp a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
				}
				else {
					System.out.println("L�trehoz�s sikertelen.");
					appendOutputToFile("L�trehoz�s sikertelen.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addDwarf> <hp> <intX> <intY> <intOffset>");
		}
	}

	/**
	 * Hobbit ellens�g l�trehoz�sa parancs, adott �leter�vel adott helyre. 
	 * A megfelel� hely�t az x �s y koordin�t�k adj�k meg, az �ton bel�li hely�t a negyedik param�ter �rt�k adja (eltol�s). 
	 * @param splits
	 */
	private void addHobbit(String[] splits) {
		if(splits.length==5){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4],7) ){
				appendCommandsToFile(splits);
				int i = 0;
				String segeds = "Routesegment("+splits[2]+","+splits[3]+","+splits[4]+")";
				while (!(segeds.equals(segedRouteSegments.get(i).name))&&(i<segedRouteSegments.size()-1)){
					i++;
				}
				if (i<segedRouteSegments.size()-1){
					Hobbit hobbit = new Hobbit(Integer.valueOf(splits[1]),creepList,segedRouteSegments.get(i),segedRouteSegments.get(i).getSpeed());
					creepList.addCreep(hobbit);
					segedRouteSegments.get(i).addCreep(hobbit);
					System.out.println("L�trehozva egy "+splits[1]+" �leterej� hobbit a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
					appendOutputToFile("L�trehozva egy "+splits[1]+" �leterej� hobbit a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
				}
				else {
					System.out.println("L�trehoz�s sikertelen.");
					appendOutputToFile("L�trehoz�s sikertelen.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addHobbit> <hp> <intX> <intY> <intOffset>");
		}	
	}

	/**
	 * Ember ellens�g l�trehoz�sa parancs, adott �leter�vel adott helyre. 
	 * A megfelel� hely�t az x �s y koordin�t�k adj�k meg, az �ton bel�li hely�t a negyedik param�ter �rt�k adja (eltol�s). 
	 * @param splits
	 */
	private void addHuman(String[] splits) {
		if(splits.length==5){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4],7) ){
				appendCommandsToFile(splits);
				int i = 0;
				String segeds = "Routesegment("+splits[2]+","+splits[3]+","+splits[4]+")";
				while (!(segeds.equals(segedRouteSegments.get(i).name))&&(i<segedRouteSegments.size()-1)){
					i++;
				}
				if (i<segedRouteSegments.size()-1){
					Human human = new Human(Integer.valueOf(splits[1]),creepList,segedRouteSegments.get(i),segedRouteSegments.get(i).getSpeed());
					creepList.addCreep(human);
					segedRouteSegments.get(i).addCreep(human);
					System.out.println("L�trehozva egy "+splits[1]+" �leterej� ember a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
					appendOutputToFile("L�trehozva egy "+splits[1]+" �leterej� ember a/az "+splits[2]+" "+splits[3]+" "+splits[4]+" helyen.");
				}
				else {
					System.out.println("L�trehoz�s sikertelen.");
					appendOutputToFile("L�trehoz�s sikertelen.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <addHuman> <hp> <intX> <intY> <intOffset>");
		}
	}


	/**
	 * �takad�ly �p�t�se, melynek els� param�tere az �ra (mana), m�sodik �s harmadik param�tere az adott hely,
	 *  ahova az akad�ly �p�l (X �s Y koordin�t�k).
	 * @param splits
	 */
	private void buildRoadBlock(String[] splits) {
		if(splits.length==4){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3])){
				appendCommandsToFile(splits);
				int i = 0;
				while (!((Integer.valueOf(splits[2])==segedFields.get(i).x)&&(Integer.valueOf(splits[3])==segedFields.get(i).y))&&(i<segedFields.size()-1)){
					i++;
				}
				if (i<segedFields.size()-1){
					player.setActiveField(segedFields.get(i));
					roadblockMana = Integer.valueOf(splits[1]);
					player.buildRoadBlock();
				}
				else {
					System.out.println("Nincs ilyen koordin�t�j� mez�.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <buildRoadBlock> <mana> <intX> <intY>");
		}		
	}

	/**
	 * Torony �p�t�se parancs, els� param�tere az �ra (mana), 
	 * m�sodik �s harmadik param�tere az adott hely, ahova a torony �p�l, (X �s Y koordin�t�k). 
	 * Negyedik param�tere a torony sebz�se, majd sebess�g �s hat�t�vols�g param�ter.
	 * @param splits
	 */
	private void buildTower(String[] splits) {
		if(splits.length==7){
			if(checkInt(splits[1]) && checkInt(splits[2]) && checkInt(splits[3]) && checkInt(splits[4]) && checkInt(splits[5]) && checkInt(splits[6]) ){
				appendCommandsToFile(splits);
				int i = 0;
				while (!((Integer.valueOf(splits[2])==segedFields.get(i).x)&&(Integer.valueOf(splits[3])==segedFields.get(i).y))&&(i<segedFields.size()-1)){
					i++;
				}
				if (i<segedFields.size()-1){
					player.setActiveField(segedFields.get(i));
					towerMana = Integer.valueOf(splits[1]);
					player.buildTower(Integer.valueOf(splits[4]),Integer.valueOf(splits[5]),Integer.valueOf(splits[6]));
				}
				else {
					System.out.println("Nincs ilyen koordin�t�j� mez�.");
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <buildTower> <mana> <intX> <intY> <damage> <firerate> <range>");
		}
	}

	/**
	 * Bet�lti a j�t�k ter�let�t. Minden �tnak a negyedik param�terben megadott �rt�k lesz az rajta �thalad� creep sebess�ge. 
	 * Az id�z�t�t null�ba �ll�tja, t�rli az ellens�g �s toronylist�t, legener�lja a map-ot � megfelel egy newgame parancsnak is egyben.
	 * @param splits
	 */
	private void mapLoad(String[] splits) {
		if(splits.length==5){
			if( checkInt(splits[4])){
				appendCommandsToFile(splits);
				map_data_Filename = splits[1];
				route_data_Filename = splits[2];
				adjacency_Filename = splits[3];
				timer.set(0);
				speed = Integer.valueOf(splits[4]);
				blocked_speed = speed * 2;
				towerList.deleteAll();
				if (mapData.initialize(map_data_Filename,route_data_Filename,adjacency_Filename) == 0){
					System.out.println("A file-ok sikeresen bet�ltve.");
					appendOutputToFile("A file-ok sikeresen bet�ltve.");	
					creepList.initialize();
					/**
					 * nincs k�d, v�letlen k�dhossz, mindig �szrevesz k�d eset�n, v�letlenszer� �tvonalv�laszt�s, nincs oszt�d�s
					 */
					fog = 2;
					fog_time = 0;
					covered = 1;
					move_seek = 0;
					multiplied = 2;
					timer.set(roundTime);
				}
			}
			else{
				System.out.println("A param�ter nem felel meg az el��r�soknak.");
			}
		}
		else{
			System.out.println("Hiba, az elv�rt bemenet: <mapLoad> <map_data filename> <route_data filename> <adjacency filename> <defaultSpeed>");
		}
	}

	private void prompt(){
        
        System.out.print(">> ");
        
    }

}