package soft4;

/**
 * A szoftvert haszn�l� j�t�kosnak megfelel� objektum, aminek feladata a mana t�rol�sa �s m�dos�t�sa,
 *  a �k�v�lr�l� �rkez� parancsok beilleszt�se a rendszer folyamataiba.
 *
 */
public class Player {
	/**
	 * a j�t�kos �ltal kiv�lasztott field p�ld�ny.
	 */
	private Field activeField;
	/**
	 * a j�t�kos rendelkez�sre �ll� er�forr�sa, a var�zser� sz�m�rt�ke.
	 */
	private int mana;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private MapData mapData;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private Timer timer;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private TowerList towerList;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private CreepList creepList;
	

	
	/**
	 * Konstruktor
	 * @param pmapdata mapdata p�ld�ny
	 * @param ptowerlist towerlist p�ld�ny
	 * @param pcreeplist creeplist p�ld�ny
	 * @param ptimer timer p�ld�ny
	 */
	public Player(MapData pmapdata,TowerList ptowerlist,CreepList pcreeplist,Timer ptimer) {
		activeField = null;
		mana = 0;
		mapData = pmapdata;
		towerList = ptowerlist;
		creepList = pcreeplist;
		timer = ptimer;
	}
	
	/**
	 * Akad�ly �p�t�se az activefield-re, ha van r� el�g mana.	
	 */
	public void buildRoadBlock(){
		if(activeField==null) return;
		if(mana>=PTC.roadblockMana){
			if(activeField.buildRoadBlock()){
				mana-=PTC.roadblockMana;
				System.out.println("Akad�ly fel�p�tve.");
				PTC.appendOutputToFile("Akad�ly fel�p�tve.");	
			}
		}
		else {
			System.out.println("Nincs r� el�g mana: "+mana+".");
			PTC.appendOutputToFile("Nincs r� el�g mana: "+mana+".");	
		}
	}

	/**
	 * Torony �p�t�se az activefield-re, ha van r� el�g mana.	
	 */
	public void buildTower(int dmg, int firerate, int range){
		if(activeField==null) return;
		if(mana>=PTC.towerMana){
			if(activeField.buildTower(dmg, firerate, range)){
				mana-=PTC.towerMana;
				System.out.println("A torony fel�p�tve.");
				PTC.appendOutputToFile("A torony fel�p�tve.");	
			}
		}
		else {
			System.out.println("Nincs r� el�g mana: "+mana+".");
			PTC.appendOutputToFile("Nincs r� el�g mana: "+mana+".");	
		}
	}
	
	
	/**
	 * A mana-hoz hozz�adja a param�tert.	
	 * @param value mana v�ltoztat�s�nak m�rt�ke: - cs�kkent, + n�vel
	 */
	public void changeMana(int value){
		mana+=value;
	}

	/**
	 * sebz�sm�dos�t� k� rak�sa toronyba, ha van r� el�g mana.	
	 * @param mdstn a j�t�kos �ltal kiv�lasztott sebz�sm�dos�t� k� p�ld�ny. 
	 */
	public boolean damageModifierStoneToTower(DamageModifierStone mdstn){
		if(activeField!=null){
			if(mana>=mdstn.getMana()){
				if(activeField.addDamageModifierStoneToTower(mdstn)){
					mana-=mdstn.getMana();
				}
				else {
					return false;
				}
			}
			else {
				System.out.println("Nincs r� el�g mana: "+mana+".");
				PTC.appendOutputToFile("Nincs r� el�g mana: "+mana+".");	
				return false;
			}
		}
		else {
			System.out.println("Nincs ilyen koordin�t�j� mez�.");
			return false;
		}
		return true;		
	}

	/**
	 * J�t�k v�ge, id�z�t� le�ll�t�sa.	
	 */
	public void defeat(){
		timer.set(0);
	}
		
	/**
	 * �j j�t�k ind�t�sa, aktu�lis j�t�k le�ll�t�sa, majd t�rl�s �s �jragener�l�s.
	 * A prot� tesztel�se miatt a PTC-ben kapott helyet.	
	 */
/*	public void newGame(){
		timer.set(0);
		towerList.deleteAll();
		mana = 50;
		activeField = null;
		mapData.initialize("map_data.dat","route_data.dat","adjacency.dat");
		creepList.initialize();
		timer.set(10);
	}*/

	/**
	 * A param�ter hozz�rendel�se az activefield attrib�tumhoz.	
	 * @param field a j�t�kos �ltal kiv�lasztott field p�ld�ny
	 */
	public void setActiveField(Field field){
		activeField = field;
	}

	/**
	 * K� rak�sa akad�lyba, ha van r� el�g mana.	
	 * @param rbs a j�t�kos �ltal kiv�lasztott roadblockstone p�ld�ny, amit az akad�lyhoz rendel�nk.
	 */
	public boolean stoneToRoadBlock(RoadBlockStone rbs){
		if(activeField!=null){
			if(mana>=PTC.roadblockStoneMana){
				if(activeField.addStoneToRoadBlock(rbs)){
					mana-=PTC.roadblockStoneMana;
				}
				else {
					return false;
				}
			}
			else {
				System.out.println("Nincs r� el�g mana: "+mana+".");
				PTC.appendOutputToFile("Nincs r� el�g mana: "+mana+".");	
				return false;
			}
		}
		else {
			System.out.println("Nincs ilyen koordin�t�j� mez�.");
			return false;
		}
		return true;		
	}
	
	/**
	 * Attrib�tumm�dos�t� k� rak�sa toronyba, ha van r� el�g mana.	
	 * @param bststn a j�tt�kos �ltal kiv�lasztott booststone p�ld�ny, amit a toronyhpz rendel�nk.
	 */
	public boolean stoneToTower(BoostStone bststn){
		if(activeField!=null){
			if(mana>=bststn.getMana()){
				if(activeField.addBoostStoneToTower(bststn)){
					mana-=bststn.getMana();
				}
				else {
					return false;
				}
			}
			else {
				System.out.println("Nincs r� el�g mana: "+mana+".");
				PTC.appendOutputToFile("Nincs r� el�g mana: "+mana+".");	
				return false;
			}
		}
		else {
			System.out.println("Nincs ilyen koordin�t�j� mez�.");
			return false;
		}
		return true;		
	}

	/**
	 * J�t�k v�ge, id�z�t� be�ll�t�sa, aktu�lis mana hasonl�t�sa a rekord �rt�k�hez,
	 *  ha nagyobb, �j rekord be�ll�t�sa.	
	 */
	public void victory(){
		timer.set(0);
		//TODO
	}
	
}
