package soft4;

/**
 * Olyan  <code>Field</code> , ami �tk�nt viselkedik, rendelkezik egy sebess�g �rt�kkel, ami megadja,
 *  hogy milyen gyorsan lehet �thaladni rajta, valamint akad�ly �p�thet� r�.
 * @see soft4.Field
 */
public class Road extends Field {
	/**
	 * az �ton tal�lhat� akad�ly
	 */
	private RoadBlock roadBlock;
	
	/**
	 * creep �thalad�sakor ez az �rt�k �ll�t�dik be a nextmovein attrib�tumba
	 */
	private int speed;

	/**
	 * Konstruktor
	 * alapsebess�get �ll�t.
	 */
	public Road(int aktspeed) {
		super(); 
		speed = aktspeed;
	}

	/**
	 * Toronyba attrib�tumm�dos�t� k� helyez�se.
	 * @param bs a hozz�rendelend� booststone p�ld�ny
	 * @return <code>false</code>, mivel road-ra nem �p�thet� torony 
	 */
	@Override
	public boolean addBoostStoneToTower(BoostStone bs) {
		System.out.println("Az activefield nem Buildsite.");
		PTC.appendOutputToFile("Az activefield nem Buildsite.");			
		return false;
	}

	/**
	 * Toronyba sebz�sm�dos�t� k� helyez�se.
	 * @param ms a hozz�rendelend� damagemodifierstone p�ld�ny
	 * @return <code>false</code>, mivel road-ra nem �p�thet� torony 
	 */	
	@Override
	public boolean addDamageModifierStoneToTower(DamageModifierStone ms) {
		System.out.println("Az activefield nem Buildsite.");
		PTC.appendOutputToFile("Az activefield nem Buildsite.");			
		return false;
	}

	/**
	 * Akad�lyba k� helyez�se.
	 * @param stone a hozz�rendelend� roadblockstone p�ld�ny
	 * @return <code>true</code>, ha van akad�ly �s az akad�ly "elfogadja a k�vet", <code>false</code> egy�bk�nt 
	 */	
	@Override
	public boolean addStoneToRoadBlock(RoadBlockStone stone) {
		if(roadBlock!=null) {
			return roadBlock.setStone(stone);
		}
		else {
			System.out.println("Az activefield-en nincs akad�ly.");
			PTC.appendOutputToFile("Az activefield-en nincs akad�ly.");				
			return false;
		}		
	}

	/**
	 * Torony �p�t�se a mez�re.	
	 * @return <code>false</code>, mivel road-ra nem �p�thet� torony
	 */
	@Override
	public boolean buildTower(int dmg, int firerate, int range) {
		System.out.println("Az activefield nem Buildsite.");
		PTC.appendOutputToFile("Az activefield nem Buildsite.");	
		return false;
	}

	/**
	 * Akad�ly �p�t�se a mez�re.
	 * @return <code>true</code>, ha m�g nincs akad�ly, <code>false</code> ha van	
	 */
	@Override
	public boolean buildRoadBlock() {
		if(roadBlock==null) {
			roadBlock = new RoadBlock(this);
			speed = PTC.blocked_speed;
			return true;
		}
		else {
			System.out.println("Az activefield-en m�r van akad�ly.");
			PTC.appendOutputToFile("Az activefield-en m�r van torony.");				
			return false;
		}
	}

	/**
	 * Speed lek�rdez�se.	
	 * @return <code>speed</code> attrib�tum �rt�ke
	 */
	public int getSpeed(){
		return speed;
	}

	/**
	 * Speed be�ll�t�sa.	
	 * @param i a speed attrib�tum �j �rt�ke
	 */
	public void setSpeed(int spd) {
		speed = spd;
	}
}
