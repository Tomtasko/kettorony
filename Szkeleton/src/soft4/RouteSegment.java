package soft4;

import java.awt.Point;
import java.util.ArrayList;

/**
 * A creep-ek �ltal haszn�lt �tvonalak egy eleme. Mivel a creep-ek nem teljes field-enk�nt
 *  mozognak el�re, hanem t�bb l�p�sben haladnak �t rajtuk, az �tvonal elem 2 field-et tartalmaz,
 *  valamint egy eltol�st, ami azt mutatja, hogy h�ny l�p�ssel hagyta el az adott routesegment az 1. field-et.
 *  Ismeri az ut�na k�vetkez� routesegment-et �s meg tudja mondani, hogy milyen creep van �ppen rajta.
 *  @see soft4.Creep
 *  @see soft4.Road
 *
 */
public class RouteSegment {
	/**
	 * prot� teszthez
	 */
	public String name;
	/**
	 * a k�d�s�t�
	 */
	private Weather w;
	/**
	 * a routesegment-en l�v� creep-ek list�ja.
	 */
	private ArrayList<Creep> creeps;
	/**
	 * a RouteSegment �ltal elfoglalt els� Road.
	 */
	private Road field1;
	/**
	 * a RouteSegment �ltal elfoglalt m�sodik Road.
	 */
	private Road field2;
	
	
	/**
	 * a k�vetkez� routesegment.
	 */
	/**
	 * @deprecated
	 */
	public RouteSegment next;
	/**
	 * A l�p�sek sz�ma adott ir�nyba az els� <code>Field</code>-t�l.
	 * Egyik koordin�t�ja helyes haszn�latkor 0.
	 * pl.(-1,0): felfele 1 egys�gnyire, (0,2) jobbra 2 egys�gnyire, ...
	 */
	private Point offset; 
//	private int priority;
	//a routesegment fontoss�gi �rt�ke, l�trehoz�skor ker�l be�ll�t�sra
	//(a routesegment lista hossz�t elosztjuk az aktu�lis routesegment sorsz�m�val).	
	
	/**
	 * a lehets�ges k�vetkez� mez�k list�ja
	 */
	private ArrayList<RouteSegment> nexts;

/**
 * Konstruktor	
 * @param road1 az els� �t
 * @param road2 a m�sodik �t
 * @param off  az offszet
 */
	public RouteSegment(Field road1, Field road2, Point off, Weather v) {
		creeps = new ArrayList<>();
		nexts = new ArrayList<>();
		field1 = (Road)road1;
		field2 = (Road)road2;
		offset = new Point(off);
		int seged = Math.abs((int)off.getX()+(int)off.getY());
		name = "Routesegment("+field1.x+","+field1.y+","+seged+")";
		w = v;
	}

	/**
	 * A param�terk�nt kapott creep hozz�f�z�se a creeps list�hoz.	
	 * @param creep hozz�adand� creep p�ld�ny
	 */
	public void addCreep(Creep creep){
		creeps.add(creep);
	}

	/**
	 * visszaadja a creeps lista els� elem�t.	
	 * @return a creeps lista els� eleme, vagy <code>null</code>, ha �res a lista 
	 */
	public Creep getCreep(){
		if(creeps.size()==0) return null;
		if(w.covered()) return null;
		return creeps.get(0);
	}

	/**
	 * Megh�vja a field1 �s field2 getSpeed() met�dus�t �s a kapott �rt�kek k�z�l a nagyobbat adja vissza.	
	 * @return max(field1.speed, field2.speed)
	 */
	public int getSpeed(){
		int i1 = field1.getSpeed();
		int i2 = field2.getSpeed();
		return Math.max(i1, i2);
	}
	
	/**
	 * K�vetkez� <code>RouteSegment</code> hozz�ad�sa
	 * @param rs a <code>RouteSegment</code>
	 */
	public void addNext(RouteSegment rs){
		nexts.add(rs);
	}
	
	/**
	 * A lehets�ges k�vetkez� elemek k�z�l 1 randomot visszaad
	 * @return egy k�vetkez� <code>RouteSegment</code>
	 */
	public RouteSegment getRandomNext(){
		if(nexts.size()==0) return null;
		return (nexts.get((int)Math.round(Math.random()*(nexts.size()-1))));
	}
	
	/**
	 * Az az �t, mely az �tvonalon kor�bban van.
	 * @return a <code>Road</code>
	 */
	public Road getStart(){
		return field1;
	}
	
	/**
	 * Visszaadja az �tvonalon k�s�bbi utat.
	 * @return a <code>Road</code>
	 */
	public Road getEnd(){
		return field2;
	}
	
	/**
	 * Visszaadja az �thoz k�pesti eltol�st
	 * @return az eltol�s
	 */
	public Point getOffset(){
		return offset;
	}

	/**
	 * A param�terk�nt kapott creep t�rl�se a creeps list�b�l, majd ha a next attrib�tum �rt�ke nem <code>null</code>,
	 *  az �ltala mutatott routesegment-en megh�vja az addCreep() met�dust a param�terk�nt kapott �rt�kkel.
	 *  Visszaadott �rt�k a next attrib�tum.	
	 * @param creep a mozg� creep p�ld�ny
	 * @return next attrib�tum
	 */
	public RouteSegment onMove(Creep creep){
		RouteSegment tmp = getRandomNext();
		if(tmp == null) return null;
		tmp.addCreep(creep);
		creeps.remove(creep);
		return tmp;
	}

	/**
	 * A param�terk�nt kapott creep t�rl�se a creeps list�b�l.	
	 * @param creep a t�rlend� creep p�ld�ny
	 */
	public void removeCreep(Creep creep){
		creeps.remove(creep);
	}
}
