package soft4;

/**
 * Az oszt�ly felel�ss�ge a szoftver rendszeres, automatikus folyamatainak a m�k�dtet�se,
 *  meghat�rozott fix id�k�z�nk�nt megh�vja a megfelel� oszt�lyok met�dusait.
 *
 */
public class Timer extends Thread {
	/**
	 * k�t ontimer met�dush�v�s k�z�tti id�k�z (ezred m�sodpercben).
	 */
	private int baseDelay;
	/**
	 * az eltelt id�
	 */
	private int currentTime;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private CreepList creepList;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private Player player;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private TowerList towerList;
	/**
	 * a k�d�t kezelend�
	 */
	private Weather weather;
		
	/**
	 * Konstruktor	
	 * @param p player p�ld�ny
	 * @param tl towerlist p�ld�ny
	 * @param cl creeplist p�ld�ny
	 */
	public Timer(Player p, TowerList tl, CreepList cl, Weather w) {
		super();
		player = p;
		towerList = tl;
		creepList = cl;
		baseDelay = currentTime = 0;
		weather = w;
	}

	/**
	 * Ez a met�dus indul el, ha lej�rt 1 basedelay-nyi id�.	
	 */
	public void onTimer(){
		if (baseDelay>0){
/**
 * A j�t�kos man�j�nak n�vel�se, a player man�j�t n�velj�k a k�r�nk�nt j�r� mana mennyis�g�vel. 					
 */
			player.changeMana(PTC.manabyround);
			System.out.println(PTC.tick+". k�r: Player: Mana v�ltozott "+PTC.manabyround+" �rt�kkel.");
			PTC.appendOutputToFile(PTC.tick+". k�r: Player: Mana v�ltozott "+PTC.manabyround+" �rt�kkel.");
			
/**
* A Weather time met�dus�nak h�v�sa. A time met�dus megn�zi, hogy a weather foggy attrib�tuma 0 vagy pedig egy 0-n�l nagyobb sz�m.
* Ha 0, sorsol egy random sz�mot, ha az beleesik a megadott �rt�ktartom�nyba, akkor a k�d leereszkedik, random hossz� id�tartamra (a sorsolt random �rt�k beker�l a foggy attrib�tumba.
* Ha nem 0, akkor cs�kkentj�k 1-el a foggy attrib�tum �rt�k�t.
*/
			weather.time();
			
/**
 * Ellens�gek mozgat�sa, ehhez megh�vjuk a creeplist moveAll() met�dus�t, ami a creeps lista minden elem�n�l megh�vja a move() met�dust:
 * 1.	A nextmovein attrib�tum �rt�k�nek cs�kkent�se 1-el, ha m�g nagyobb, mint 0, a met�dus v�get �rt. 
 * 2.	Ha a nextmovein 0 lett, megh�vjuk a Creep aktu�lis poz�ci�j�n (activesegment attrib�tum) az onMove() met�dust, param�terk�nt a Creep p�ld�nnyal, aki �ppen l�p. Az onMove() lek�rdezi az activesegment next attrib�tum�nak az �rt�k�t (el�gaz�skor itt nem 1, hanem t�bb van felsorolva, ezek k�z�l v�lasztunk v�letlenszer�en), ha az NULL, v�get �rt a met�dus. 
 * 3.	Ha az activesegment next attrib�tuma nem NULL, t�r�lj�k a creeps list�j�b�l a kapott Creep p�ld�nyt, majd a mutatott Routesegment p�ld�nyon megh�vjuk az addCreep() met�dust, param�terk�nt az �ppen l�p� Creep p�ld�nnyal. A met�dus hozz�f�zi a Routesegment creeps list�j�hoz a kapott param�tert. 
 * 4.	Az onMove() met�dush�v�s visszaadott �rt�ke a k�vetkez� Routesegment, vagy NULL, ha nincs t�bb, ezt az activesegment attrib�tumban t�roljuk el. 
 * 5.	Ha a kapott �rt�k nem NULL: Megh�vjuk az activesegment getSpeed() met�dus�t, ami ennek hat�s�ra lek�ri a field1 �s field2, Road t�pus� attrib�tumainak a sebess�g�t a getSpeed() met�dusok megh�v�s�val. A kapott k�t �rt�k k�z�l a nagyobbikat adjuk vissza. A kapott �rt�ket elt�roljuk a nextmovein attrib�tumban, ezut�n a met�dus v�get �rt. 
 * 6.	Ha a kapott �rt�k NULL: A j�t�k v�get �rt, megh�vjuk a player defeat() met�dus�t, ami le�ll�tja a j�t�kot a timer set() met�dus�nak megh�v�s�val, 0-s param�terrel. 
 */
			creepList.moveAll();
			
/**
 * Tornyok t�zel�se, ehhez megh�vjuk a towerlist fireAll() met�dus�t, ami a towers lista minden elem�n�l megh�vja a fire() met�dust: 
 * 1.	A nextfirein attrib�tum �rt�k�nek cs�kkent�se 1-el, ha m�g nagyobb, mint 0, a met�dus v�get �rt. 
 * 2.	Ha a nextfirein 0 lett, fel�l�rjuk az �rt�k�t firerate attrib�tum �rt�k�vel. 
 * 3.	V�gigmegy a routesegments lista minden elem�n �s megh�vja a getCreep() met�dusokat, aminek a visszat�r�si �rt�k 1 Creep (a Routesegment p�ld�ny creeps list�j�nak 1. eleme), vagy NULL, ha nincs creep rajta. Amennyiben az �ppen lek�rdezett routesegment-en van creep, lek�rdezz�k a Weather oszt�ly covered() met�dus�val, hogy visszaadja-e a creep-et (ha igazat kap, visszaadja, ha hamisat, akkor nem). A lek�rdez�s addig fut, am�g nem kap vissza 1 Creep-et, vagy v�gig nem �rt�nk a list�n. 
 * 4.	Ha v�gig�rt�nk a list�n, a met�dus v�get �rt. 
 * 5.	Ha van visszaadott Creep, megh�vja a hurt() met�dus�t, param�terk�nt a damage �s a damagemodifier attrib�tumokkal. 
 * 6.	A hurt() met�dush�v�s hat�s�ra a Creep p�ld�ny megh�vja, p�ld�ny�t�l f�gg�en a kapott ammo (DamageModifierStone p�ld�ny) damageModifierOnDwarf(), damageModifierOnElf(), damageModifierOnHobbit(), vagy damageModifierOnHuman() met�dusok egyik�t, majd a kapott sz�mot felszorozza a damage param�ter �rt�k�vel.
 * 7.	Ezut�n j�n egy v�letlensz�m gener�l�s, aminek az eredm�nye alapj�n d�ntj�k el, hogy a creep norm�lisan sebz�dik, vagy oszt�dik.
 * 8.	Ha sebz�dik, az �gy l�trej�tt �rt�ket levonja a hp attrib�tum�nak �rt�k�b�l. 
 * 	a.	Ha a hp attrib�tum �rt�ke nagyobb, mint 0, a met�dus v�get �rt. 
 * 	b.	Ha a hp attrib�tum �rt�ke 0, vagy kisebb, megh�vja az activesegment attrib�tum�nak a removeCreep() met�dus�t, param�terk�nt saj�t mag�val, aminek hat�s�ra az t�rli a Creep-et a creeps list�j�b�l. 
 * 	c.	Ezek ut�n megh�vja a creeplist imDead() met�dus�t, param�terk�nt saj�t mag�val, aminek hat�s�ra a creeplist deads list�j�hoz hozz�f�zi a kapott �rt�ket. 
 * 9.	Ha oszt�dik, megfelezi a hp-t (ha 1, akkor marad 1).
 * 	a.	Lem�solja mag�t.
 * 	b.	A m�solatot �tadja a creeplist-nek t�rol�sra az addCreep() met�dussal.
 * 	c.	A m�solatot �tadja az aktu�lis routesegment-j�nek az addCreep() met�dussal.
 */
			towerList.fireAll();

/**
 * Halott creep-ek t�rl�se: A halottak elt�vol�t�s�t a creeplist removeDead() met�dus�val v�gezz�k el, a l�p�sek: 
 * 1.	V�gigmegy a deads lista minden elem�n: 
 *  a.	Kikeresi a creeps list�b�l. 
 *  b.	Az aktu�lis listaelem�rt (Creep p�ld�ny) j�r� man�t hozz�adja a player man�j�hoz a player changeMana() met�dus�val, param�terk�nt a mana mennyis�g�t adjuk �t. 
 *  c.	A listaelemet t�r�lj�k mindk�t klist�b�l �s a Creep p�ld�nyt is t�r�lj�k. 
 * 2.	Ha a creeps lista �res lett, a j�t�kos nyert, megh�vjuk a player Victory() met�dus�t.
 * 3.	A Victory() met�dus le�ll�tja a j�t�kot a timer set() met�dus�nak megh�v�s�val, 0-s param�terrel. 
 */
			//creepList.removeDead();
		}
	}

	/**
	 * Basedelay be�ll�t�sa	
	 * @param i a basedelay �j �rt�ke
	 */
	public void set(int i){
		baseDelay=i;
	}
	
	/**
	 * nem lehet k�l�nben lekonstruktorolni
	 * @param p
	 */
	public void setPlayer(Player p){
		player = p;
	}
	
	/**
	 * visszasz�ml�l
	 */
	public void run(){
		while(baseDelay!=0){
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		currentTime-=1;
		System.out.println(currentTime);
		if(currentTime<=0){
			currentTime=baseDelay;
			onTimer();
		}
		}
	}
}
