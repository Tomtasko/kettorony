package soft4;

import java.util.ArrayList;
/**
 * 
 * A map-ra �p�tett towerek list�ja, a list�hoz tov�bbi tower ad�sa, a lista t�rl�se,
 *  valamint a towerek m�k�dtet�se.
 *
 */
public class TowerList {
	/**
	 * towereket tartalmaz� lista.
	 */
	private ArrayList<Tower> towers;

	/**
	 * Konstruktor, �res lista l�trehoz�sa.	
	 */
	public TowerList() {
		towers = new ArrayList<>();
	}

	/**
	 * A param�terk�nt kapott tower hozz�ad�sa a list�hoz.	
	 * @param tower a hozz�adand� tower p�ld�ny.
	 */
	public void addTower(Tower tower){
		towers.add(tower);
		tower.name = "Tower " + towers.size()+": ";
	}

	/**
	 * A towers lista t�rl�se.	
	 */
	public void deleteAll(){
		towers.clear();
	}

	/**
	 * A lista �sszes torny�n a Fire() met�dus megh�v�sa.	
	 */
	public void fireAll(){
		for(Tower t: towers){
			t.fire();
		}
	}
}
