package soft4;

/**
 * A k�d�s�t�st v�gz� oszt�ly
 * @author Povazson
 *
 */
public class Weather {
	/**
	 * a k�d h�tral�v� id�tartama
	 * ha nincs k�d, 0
	 */
	private int foggy;
	
	/**
	 * init
	 */
	public void init(){
		foggy = 0;
	}
	
	/**
	 * k�d gener�l�sa ha kell, ha nem cs�kkenti 1 el a visszamarad� �rt�k�t
	 */
	public void time(){
		switch (PTC.fog) {
			case 0: if(foggy ==0){
						int rnd = (int)Math.round(Math.random()*100);
						if(rnd>=85){
							if (PTC.fog_time==0){
								foggy = (int)Math.round(Math.random()*25);	
							}
							else {
								foggy = PTC.fog_time;
							}
							System.out.println(PTC.tick+". k�r: K�d k�pz�d�tt "+foggy+" k�rre.");
							PTC.appendOutputToFile(PTC.tick+". k�r: K�d k�pz�d�tt "+foggy+" k�rre.");							
						}
					}
					else{
						foggy--;
						if (foggy==0) {
							System.out.println(PTC.tick+". k�r: K�d felsz�llt.");
							PTC.appendOutputToFile(PTC.tick+". k�r: K�d felsz�llt.");					
						}
					}
					break;
			case 1: if(foggy ==0){
						if (PTC.fog_time==0){
							foggy = (int)Math.round(Math.random()*25);	
						}
						else {
							foggy = PTC.fog_time;
						}
						
						System.out.println(PTC.tick+". k�r: K�d k�pz�d�tt "+foggy+" k�rre.");
						PTC.appendOutputToFile(PTC.tick+". k�r: K�d k�pz�d�tt "+foggy+" k�rre.");							
					}
					else{
						foggy--;
						if (foggy==0) {
							System.out.println(PTC.tick+". k�r: K�d felsz�llt.");
							PTC.appendOutputToFile(PTC.tick+". k�r: K�d felsz�llt.");					
						}
					}
					break; 
			case 2: foggy = 0;
					break;
		}
	}
	
	/**
	 * Megadja, hogy az adott szegmens visszaadhat e creepet, vagy el vannak takarva a k�d �ltal.
	 * A l�that�s�got k�d eset�n v�letlen sz�m d�nti el.
	 * @return <code>true</code> ha nem l�that�, <code>false</code> ha nincs k�d, vagy ha l�that�k.
	 */
	public boolean covered(){
		if(foggy==0) return false;
		int rnd = (int)(Math.round(Math.random()*100));
		if(rnd >=60) return true;
		return false;
	}
}
